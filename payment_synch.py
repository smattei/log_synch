from paramiko import SSHClient
from scp import SCPClient
import socket
import sys
import os
from os import listdir
from os.path import isfile, join
import datetime

try:
    hostname = sys.argv[1]
except IndexError:
    print("hostname required")
    exit(2)

local_host = socket.gethostname()

ssh = SSHClient()
ssh.load_system_host_keys()
try:
    ssh.connect(hostname, username='root', password='RXesvHNF42')
    sftp = ssh.open_sftp()
except Exception:
    exit(2)

local_nginx_pwd = "/var/log/payment_gateway/"
remote_nginx_pwd = "/share/backup/dump/"
today = "-" + str(datetime.date.today())
sftp.chdir(remote_nginx_pwd)


nginx_logs = [f for f in listdir(
    local_nginx_pwd) if f.endswith(".log.1") if isfile(join(local_nginx_pwd, f))]

with SCPClient(ssh.get_transport()) as scp:
    for log in nginx_logs:
        filename = local_host + "_" + log
        try:
            if os.stat(local_nginx_pwd + log).st_size != sftp.stat(remote_nginx_pwd + filename + today).st_size:
                scp.put(local_nginx_pwd + log, remote_nginx_pwd +
                        filename + today)
        except FileNotFoundError:
            scp.put(local_nginx_pwd + log, remote_nginx_pwd +
                    filename + today)
