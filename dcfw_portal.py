from paramiko import SSHClient
import sys
import datetime


fw_list = ["fw01", "fw02", "fw03", "fw04"]
local_nginx_pwd = "/var/log/nginx/"
remote_nginx_pwd = "/var/log/nginx/"

for host in fw_list:
    ssh = SSHClient()
    ssh.load_system_host_keys()
    ssh.connect(host)
    sftp = ssh.open_sftp()
    sftp.chdir(local_nginx_pwd)

    try:
        hostname = sys.argv[1]
    except IndexError:
        print("hostname required")
        exit(2)

    try:
        ssh2 = SSHClient()
        ssh2.load_system_host_keys()
        ssh2.connect(hostname, username='webzine', password='RSEiZ3b')
        sftp2 = ssh2.open_sftp()
    except Exception:
        exit(2)

    today = "-" + str(datetime.date.today())
    sftp2.chdir(remote_nginx_pwd)

    nginx_logs1 = [f for f in sftp.listdir() if f.endswith(
        "goodbarber.com.log.1")]
    nginx_logs2 = [f for f in sftp.listdir() if f.startswith(
        "www.goodbarber.")]

    for log in nginx_logs1:
        filename = host + "_" + log
        try:
            if sftp.lstat(log).st_size != sftp2.lstat(remote_nginx_pwd + filename + today).st_size:
                ssh.exec_command(
                    "scp " + local_nginx_pwd + log + " webzine@" + hostname + ":/" + remote_nginx_pwd + filename + today)
        except FileNotFoundError:
            ssh.exec_command(
                "scp " + local_nginx_pwd + log + " webzine@" + hostname + ":/" + remote_nginx_pwd + filename + today)
    for log in nginx_logs2:
        filename = host + "_" + log
        try:
            if sftp.lstat(log).st_size != sftp2.lstat(remote_nginx_pwd + filename + today).st_size:
                ssh.exec_command(
                    "scp " + local_nginx_pwd + log + " webzine@" + hostname + ":/" + remote_nginx_pwd + filename + today)
        except FileNotFoundError:
            ssh.exec_command(
                "scp " + local_nginx_pwd + log + " webzine@" + hostname + ":/" + remote_nginx_pwd + filename + today)
